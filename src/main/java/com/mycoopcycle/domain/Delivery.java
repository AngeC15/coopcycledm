package com.mycoopcycle.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Delivery.
 */
@Entity
@Table(name = "delivery")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Delivery implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "city")
    private String city;

    @Column(name = "state_province")
    private String stateProvince;

    @Column(name = "paid")
    private Boolean paid;

    @Min(value = 0L)
    @Column(name = "trip_price")
    private Long tripPrice;

    @OneToMany(mappedBy = "areIn")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "isIns", "isAvailableIn", "areIn" }, allowSetters = true)
    private Set<Products> have = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "possesses", "restaurant", "areIn", "lead", "own", "creations" }, allowSetters = true)
    private AUser creator;

    @JsonIgnoreProperties(value = { "delivery", "bies", "choosen" }, allowSetters = true)
    @OneToOne(mappedBy = "delivery")
    private Basket basket;

    @OneToMany(mappedBy = "owns")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "owns" }, allowSetters = true)
    private Set<Bank> interrogates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Delivery id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetAddress() {
        return this.streetAddress;
    }

    public Delivery streetAddress(String streetAddress) {
        this.setStreetAddress(streetAddress);
        return this;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public Delivery postalCode(String postalCode) {
        this.setPostalCode(postalCode);
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return this.city;
    }

    public Delivery city(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return this.stateProvince;
    }

    public Delivery stateProvince(String stateProvince) {
        this.setStateProvince(stateProvince);
        return this;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public Boolean getPaid() {
        return this.paid;
    }

    public Delivery paid(Boolean paid) {
        this.setPaid(paid);
        return this;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Long getTripPrice() {
        return this.tripPrice;
    }

    public Delivery tripPrice(Long tripPrice) {
        this.setTripPrice(tripPrice);
        return this;
    }

    public void setTripPrice(Long tripPrice) {
        this.tripPrice = tripPrice;
    }

    public Set<Products> getHave() {
        return this.have;
    }

    public void setHave(Set<Products> products) {
        if (this.have != null) {
            this.have.forEach(i -> i.setAreIn(null));
        }
        if (products != null) {
            products.forEach(i -> i.setAreIn(this));
        }
        this.have = products;
    }

    public Delivery have(Set<Products> products) {
        this.setHave(products);
        return this;
    }

    public Delivery addHas(Products products) {
        this.have.add(products);
        products.setAreIn(this);
        return this;
    }

    public Delivery removeHas(Products products) {
        this.have.remove(products);
        products.setAreIn(null);
        return this;
    }

    public AUser getCreator() {
        return this.creator;
    }

    public void setCreator(AUser aUser) {
        this.creator = aUser;
    }

    public Delivery creator(AUser aUser) {
        this.setCreator(aUser);
        return this;
    }

    public Basket getBasket() {
        return this.basket;
    }

    public void setBasket(Basket basket) {
        if (this.basket != null) {
            this.basket.setDelivery(null);
        }
        if (basket != null) {
            basket.setDelivery(this);
        }
        this.basket = basket;
    }

    public Delivery basket(Basket basket) {
        this.setBasket(basket);
        return this;
    }

    public Set<Bank> getInterrogates() {
        return this.interrogates;
    }

    public void setInterrogates(Set<Bank> banks) {
        if (this.interrogates != null) {
            this.interrogates.forEach(i -> i.setOwns(null));
        }
        if (banks != null) {
            banks.forEach(i -> i.setOwns(this));
        }
        this.interrogates = banks;
    }

    public Delivery interrogates(Set<Bank> banks) {
        this.setInterrogates(banks);
        return this;
    }

    public Delivery addInterrogate(Bank bank) {
        this.interrogates.add(bank);
        bank.setOwns(this);
        return this;
    }

    public Delivery removeInterrogate(Bank bank) {
        this.interrogates.remove(bank);
        bank.setOwns(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Delivery)) {
            return false;
        }
        return id != null && id.equals(((Delivery) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Delivery{" +
            "id=" + getId() +
            ", streetAddress='" + getStreetAddress() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", city='" + getCity() + "'" +
            ", stateProvince='" + getStateProvince() + "'" +
            ", paid='" + getPaid() + "'" +
            ", tripPrice=" + getTripPrice() +
            "}";
    }
}
