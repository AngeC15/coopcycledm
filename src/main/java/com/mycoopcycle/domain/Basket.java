package com.mycoopcycle.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Basket.
 */
@Entity
@Table(name = "basket")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Basket implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "creation_date")
    private Instant creationDate;

    @Min(value = 0L)
    @Column(name = "price")
    private Long price;

    @JsonIgnoreProperties(value = { "have", "creator", "basket", "interrogates" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Delivery delivery;

    @OneToMany(mappedBy = "own")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "possesses", "restaurant", "areIn", "lead", "own", "creations" }, allowSetters = true)
    private Set<AUser> bies = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "isIns", "isAvailableIn", "areIn" }, allowSetters = true)
    private Products choosen;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Basket id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreationDate() {
        return this.creationDate;
    }

    public Basket creationDate(Instant creationDate) {
        this.setCreationDate(creationDate);
        return this;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public Long getPrice() {
        return this.price;
    }

    public Basket price(Long price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Delivery getDelivery() {
        return this.delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Basket delivery(Delivery delivery) {
        this.setDelivery(delivery);
        return this;
    }

    public Set<AUser> getBies() {
        return this.bies;
    }

    public void setBies(Set<AUser> aUsers) {
        if (this.bies != null) {
            this.bies.forEach(i -> i.setOwn(null));
        }
        if (aUsers != null) {
            aUsers.forEach(i -> i.setOwn(this));
        }
        this.bies = aUsers;
    }

    public Basket bies(Set<AUser> aUsers) {
        this.setBies(aUsers);
        return this;
    }

    public Basket addBy(AUser aUser) {
        this.bies.add(aUser);
        aUser.setOwn(this);
        return this;
    }

    public Basket removeBy(AUser aUser) {
        this.bies.remove(aUser);
        aUser.setOwn(null);
        return this;
    }

    public Products getChoosen() {
        return this.choosen;
    }

    public void setChoosen(Products products) {
        this.choosen = products;
    }

    public Basket choosen(Products products) {
        this.setChoosen(products);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Basket)) {
            return false;
        }
        return id != null && id.equals(((Basket) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Basket{" +
            "id=" + getId() +
            ", creationDate='" + getCreationDate() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
