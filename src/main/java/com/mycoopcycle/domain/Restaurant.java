package com.mycoopcycle.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Restaurant.
 */
@Entity
@Table(name = "restaurant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "city")
    private String city;

    @Column(name = "state_province")
    private String stateProvince;

    @JsonIgnoreProperties(value = { "isIns", "isAvailableIn", "areIn" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Products offer;

    @OneToMany(mappedBy = "restaurant")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "possesses", "restaurant", "areIn", "lead", "own", "creations" }, allowSetters = true)
    private Set<AUser> aUsers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "have", "members", "leaders" }, allowSetters = true)
    private Cooperative belongs;

    @OneToMany(mappedBy = "possesses")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "possesses", "restaurant", "areIn", "lead", "own", "creations" }, allowSetters = true)
    private Set<AUser> owners = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Restaurant id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetAddress() {
        return this.streetAddress;
    }

    public Restaurant streetAddress(String streetAddress) {
        this.setStreetAddress(streetAddress);
        return this;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public Restaurant postalCode(String postalCode) {
        this.setPostalCode(postalCode);
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return this.city;
    }

    public Restaurant city(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return this.stateProvince;
    }

    public Restaurant stateProvince(String stateProvince) {
        this.setStateProvince(stateProvince);
        return this;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public Products getOffer() {
        return this.offer;
    }

    public void setOffer(Products products) {
        this.offer = products;
    }

    public Restaurant offer(Products products) {
        this.setOffer(products);
        return this;
    }

    public Set<AUser> getAUsers() {
        return this.aUsers;
    }

    public void setAUsers(Set<AUser> aUsers) {
        if (this.aUsers != null) {
            this.aUsers.forEach(i -> i.setRestaurant(null));
        }
        if (aUsers != null) {
            aUsers.forEach(i -> i.setRestaurant(this));
        }
        this.aUsers = aUsers;
    }

    public Restaurant aUsers(Set<AUser> aUsers) {
        this.setAUsers(aUsers);
        return this;
    }

    public Restaurant addAUser(AUser aUser) {
        this.aUsers.add(aUser);
        aUser.setRestaurant(this);
        return this;
    }

    public Restaurant removeAUser(AUser aUser) {
        this.aUsers.remove(aUser);
        aUser.setRestaurant(null);
        return this;
    }

    public Cooperative getBelongs() {
        return this.belongs;
    }

    public void setBelongs(Cooperative cooperative) {
        this.belongs = cooperative;
    }

    public Restaurant belongs(Cooperative cooperative) {
        this.setBelongs(cooperative);
        return this;
    }

    public Set<AUser> getOwners() {
        return this.owners;
    }

    public void setOwners(Set<AUser> aUsers) {
        if (this.owners != null) {
            this.owners.forEach(i -> i.setPossesses(null));
        }
        if (aUsers != null) {
            aUsers.forEach(i -> i.setPossesses(this));
        }
        this.owners = aUsers;
    }

    public Restaurant owners(Set<AUser> aUsers) {
        this.setOwners(aUsers);
        return this;
    }

    public Restaurant addOwner(AUser aUser) {
        this.owners.add(aUser);
        aUser.setPossesses(this);
        return this;
    }

    public Restaurant removeOwner(AUser aUser) {
        this.owners.remove(aUser);
        aUser.setPossesses(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Restaurant)) {
            return false;
        }
        return id != null && id.equals(((Restaurant) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Restaurant{" +
            "id=" + getId() +
            ", streetAddress='" + getStreetAddress() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", city='" + getCity() + "'" +
            ", stateProvince='" + getStateProvince() + "'" +
            "}";
    }
}
