package com.mycoopcycle.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A AUser.
 */
@Entity
@Table(name = "a_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "email")
    private String email;

    @Pattern(regexp = "^(?:(?:\\+|00)33[\\s.-]{0,3}(?:\\(0\\)[\\s.-]{0,3})?|0)[1-9](?:(?:[\\s.-]?\\d{2}){4}|\\d{2}(?:[\\s.-]?\\d{3}){2})$")
    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToOne
    @JsonIgnoreProperties(value = { "offer", "aUsers", "belongs", "owners" }, allowSetters = true)
    private Restaurant possesses;

    @ManyToOne
    @JsonIgnoreProperties(value = { "offer", "aUsers", "belongs", "owners" }, allowSetters = true)
    private Restaurant restaurant;

    @ManyToOne
    @JsonIgnoreProperties(value = { "have", "members", "leaders" }, allowSetters = true)
    private Cooperative areIn;

    @ManyToOne
    @JsonIgnoreProperties(value = { "have", "members", "leaders" }, allowSetters = true)
    private Cooperative lead;

    @ManyToOne
    @JsonIgnoreProperties(value = { "delivery", "bies", "choosen" }, allowSetters = true)
    private Basket own;

    @OneToMany(mappedBy = "creator")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "have", "creator", "basket", "interrogates" }, allowSetters = true)
    private Set<Delivery> creations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AUser id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public AUser name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public AUser surname(String surname) {
        this.setSurname(surname);
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return this.email;
    }

    public AUser email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public AUser phoneNumber(String phoneNumber) {
        this.setPhoneNumber(phoneNumber);
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Restaurant getPossesses() {
        return this.possesses;
    }

    public void setPossesses(Restaurant restaurant) {
        this.possesses = restaurant;
    }

    public AUser possesses(Restaurant restaurant) {
        this.setPossesses(restaurant);
        return this;
    }

    public Restaurant getRestaurant() {
        return this.restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public AUser restaurant(Restaurant restaurant) {
        this.setRestaurant(restaurant);
        return this;
    }

    public Cooperative getAreIn() {
        return this.areIn;
    }

    public void setAreIn(Cooperative cooperative) {
        this.areIn = cooperative;
    }

    public AUser areIn(Cooperative cooperative) {
        this.setAreIn(cooperative);
        return this;
    }

    public Cooperative getLead() {
        return this.lead;
    }

    public void setLead(Cooperative cooperative) {
        this.lead = cooperative;
    }

    public AUser lead(Cooperative cooperative) {
        this.setLead(cooperative);
        return this;
    }

    public Basket getOwn() {
        return this.own;
    }

    public void setOwn(Basket basket) {
        this.own = basket;
    }

    public AUser own(Basket basket) {
        this.setOwn(basket);
        return this;
    }

    public Set<Delivery> getCreations() {
        return this.creations;
    }

    public void setCreations(Set<Delivery> deliveries) {
        if (this.creations != null) {
            this.creations.forEach(i -> i.setCreator(null));
        }
        if (deliveries != null) {
            deliveries.forEach(i -> i.setCreator(this));
        }
        this.creations = deliveries;
    }

    public AUser creations(Set<Delivery> deliveries) {
        this.setCreations(deliveries);
        return this;
    }

    public AUser addCreation(Delivery delivery) {
        this.creations.add(delivery);
        delivery.setCreator(this);
        return this;
    }

    public AUser removeCreation(Delivery delivery) {
        this.creations.remove(delivery);
        delivery.setCreator(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AUser)) {
            return false;
        }
        return id != null && id.equals(((AUser) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AUser{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            "}";
    }
}
