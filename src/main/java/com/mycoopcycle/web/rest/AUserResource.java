package com.mycoopcycle.web.rest;

import com.mycoopcycle.repository.AUserRepository;
import com.mycoopcycle.service.AUserService;
import com.mycoopcycle.service.dto.AUserDTO;
import com.mycoopcycle.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycoopcycle.domain.AUser}.
 */
@RestController
@RequestMapping("/api")
public class AUserResource {

    private final Logger log = LoggerFactory.getLogger(AUserResource.class);

    private static final String ENTITY_NAME = "aUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AUserService aUserService;

    private final AUserRepository aUserRepository;

    public AUserResource(AUserService aUserService, AUserRepository aUserRepository) {
        this.aUserService = aUserService;
        this.aUserRepository = aUserRepository;
    }

    /**
     * {@code POST  /a-users} : Create a new aUser.
     *
     * @param aUserDTO the aUserDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aUserDTO, or with status {@code 400 (Bad Request)} if the aUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/a-users")
    public ResponseEntity<AUserDTO> createAUser(@Valid @RequestBody AUserDTO aUserDTO) throws URISyntaxException {
        log.debug("REST request to save AUser : {}", aUserDTO);
        if (aUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new aUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AUserDTO result = aUserService.save(aUserDTO);
        return ResponseEntity
            .created(new URI("/api/a-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /a-users/:id} : Updates an existing aUser.
     *
     * @param id the id of the aUserDTO to save.
     * @param aUserDTO the aUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aUserDTO,
     * or with status {@code 400 (Bad Request)} if the aUserDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/a-users/{id}")
    public ResponseEntity<AUserDTO> updateAUser(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody AUserDTO aUserDTO
    ) throws URISyntaxException {
        log.debug("REST request to update AUser : {}, {}", id, aUserDTO);
        if (aUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, aUserDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AUserDTO result = aUserService.update(aUserDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, aUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /a-users/:id} : Partial updates given fields of an existing aUser, field will ignore if it is null
     *
     * @param id the id of the aUserDTO to save.
     * @param aUserDTO the aUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aUserDTO,
     * or with status {@code 400 (Bad Request)} if the aUserDTO is not valid,
     * or with status {@code 404 (Not Found)} if the aUserDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the aUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/a-users/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AUserDTO> partialUpdateAUser(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody AUserDTO aUserDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update AUser partially : {}, {}", id, aUserDTO);
        if (aUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, aUserDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AUserDTO> result = aUserService.partialUpdate(aUserDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, aUserDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /a-users} : get all the aUsers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aUsers in body.
     */
    @GetMapping("/a-users")
    public List<AUserDTO> getAllAUsers() {
        log.debug("REST request to get all AUsers");
        return aUserService.findAll();
    }

    /**
     * {@code GET  /a-users/:id} : get the "id" aUser.
     *
     * @param id the id of the aUserDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aUserDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/a-users/{id}")
    public ResponseEntity<AUserDTO> getAUser(@PathVariable Long id) {
        log.debug("REST request to get AUser : {}", id);
        Optional<AUserDTO> aUserDTO = aUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aUserDTO);
    }

    /**
     * {@code DELETE  /a-users/:id} : delete the "id" aUser.
     *
     * @param id the id of the aUserDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/a-users/{id}")
    public ResponseEntity<Void> deleteAUser(@PathVariable Long id) {
        log.debug("REST request to delete AUser : {}", id);
        aUserService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
