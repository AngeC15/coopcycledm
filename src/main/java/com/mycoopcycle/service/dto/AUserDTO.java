package com.mycoopcycle.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mycoopcycle.domain.AUser} entity.
 */
public class AUserDTO implements Serializable {

    @NotNull
    private Long id;

    private String name;

    private String surname;

    private String email;

    @Pattern(regexp = "^(?:(?:\\+|00)33[\\s.-]{0,3}(?:\\(0\\)[\\s.-]{0,3})?|0)[1-9](?:(?:[\\s.-]?\\d{2}){4}|\\d{2}(?:[\\s.-]?\\d{3}){2})$")
    private String phoneNumber;

    private RestaurantDTO possesses;

    private RestaurantDTO restaurant;

    private CooperativeDTO areIn;

    private CooperativeDTO lead;

    private BasketDTO own;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public RestaurantDTO getPossesses() {
        return possesses;
    }

    public void setPossesses(RestaurantDTO possesses) {
        this.possesses = possesses;
    }

    public RestaurantDTO getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantDTO restaurant) {
        this.restaurant = restaurant;
    }

    public CooperativeDTO getAreIn() {
        return areIn;
    }

    public void setAreIn(CooperativeDTO areIn) {
        this.areIn = areIn;
    }

    public CooperativeDTO getLead() {
        return lead;
    }

    public void setLead(CooperativeDTO lead) {
        this.lead = lead;
    }

    public BasketDTO getOwn() {
        return own;
    }

    public void setOwn(BasketDTO own) {
        this.own = own;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AUserDTO)) {
            return false;
        }

        AUserDTO aUserDTO = (AUserDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, aUserDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AUserDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", possesses=" + getPossesses() +
            ", restaurant=" + getRestaurant() +
            ", areIn=" + getAreIn() +
            ", lead=" + getLead() +
            ", own=" + getOwn() +
            "}";
    }
}
