package com.mycoopcycle.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mycoopcycle.domain.Basket} entity.
 */
public class BasketDTO implements Serializable {

    @NotNull
    private Long id;

    private Instant creationDate;

    @Min(value = 0L)
    private Long price;

    private DeliveryDTO delivery;

    private ProductsDTO choosen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public DeliveryDTO getDelivery() {
        return delivery;
    }

    public void setDelivery(DeliveryDTO delivery) {
        this.delivery = delivery;
    }

    public ProductsDTO getChoosen() {
        return choosen;
    }

    public void setChoosen(ProductsDTO choosen) {
        this.choosen = choosen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BasketDTO)) {
            return false;
        }

        BasketDTO basketDTO = (BasketDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, basketDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BasketDTO{" +
            "id=" + getId() +
            ", creationDate='" + getCreationDate() + "'" +
            ", price=" + getPrice() +
            ", delivery=" + getDelivery() +
            ", choosen=" + getChoosen() +
            "}";
    }
}
