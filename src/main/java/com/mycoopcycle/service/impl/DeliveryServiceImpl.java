package com.mycoopcycle.service.impl;

import com.mycoopcycle.domain.Delivery;
import com.mycoopcycle.repository.DeliveryRepository;
import com.mycoopcycle.service.DeliveryService;
import com.mycoopcycle.service.dto.DeliveryDTO;
import com.mycoopcycle.service.mapper.DeliveryMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Delivery}.
 */
@Service
@Transactional
public class DeliveryServiceImpl implements DeliveryService {

    private final Logger log = LoggerFactory.getLogger(DeliveryServiceImpl.class);

    private final DeliveryRepository deliveryRepository;

    private final DeliveryMapper deliveryMapper;

    public DeliveryServiceImpl(DeliveryRepository deliveryRepository, DeliveryMapper deliveryMapper) {
        this.deliveryRepository = deliveryRepository;
        this.deliveryMapper = deliveryMapper;
    }

    @Override
    public DeliveryDTO save(DeliveryDTO deliveryDTO) {
        log.debug("Request to save Delivery : {}", deliveryDTO);
        Delivery delivery = deliveryMapper.toEntity(deliveryDTO);
        delivery = deliveryRepository.save(delivery);
        return deliveryMapper.toDto(delivery);
    }

    @Override
    public DeliveryDTO update(DeliveryDTO deliveryDTO) {
        log.debug("Request to save Delivery : {}", deliveryDTO);
        Delivery delivery = deliveryMapper.toEntity(deliveryDTO);
        delivery = deliveryRepository.save(delivery);
        return deliveryMapper.toDto(delivery);
    }

    @Override
    public Optional<DeliveryDTO> partialUpdate(DeliveryDTO deliveryDTO) {
        log.debug("Request to partially update Delivery : {}", deliveryDTO);

        return deliveryRepository
            .findById(deliveryDTO.getId())
            .map(existingDelivery -> {
                deliveryMapper.partialUpdate(existingDelivery, deliveryDTO);

                return existingDelivery;
            })
            .map(deliveryRepository::save)
            .map(deliveryMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DeliveryDTO> findAll() {
        log.debug("Request to get all Deliveries");
        return deliveryRepository.findAll().stream().map(deliveryMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get all the deliveries where Basket is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DeliveryDTO> findAllWhereBasketIsNull() {
        log.debug("Request to get all deliveries where Basket is null");
        return StreamSupport
            .stream(deliveryRepository.findAll().spliterator(), false)
            .filter(delivery -> delivery.getBasket() == null)
            .map(deliveryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DeliveryDTO> findOne(Long id) {
        log.debug("Request to get Delivery : {}", id);
        return deliveryRepository.findById(id).map(deliveryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Delivery : {}", id);
        deliveryRepository.deleteById(id);
    }
}
