package com.mycoopcycle.service.impl;

import com.mycoopcycle.domain.AUser;
import com.mycoopcycle.repository.AUserRepository;
import com.mycoopcycle.service.AUserService;
import com.mycoopcycle.service.dto.AUserDTO;
import com.mycoopcycle.service.mapper.AUserMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AUser}.
 */
@Service
@Transactional
public class AUserServiceImpl implements AUserService {

    private final Logger log = LoggerFactory.getLogger(AUserServiceImpl.class);

    private final AUserRepository aUserRepository;

    private final AUserMapper aUserMapper;

    public AUserServiceImpl(AUserRepository aUserRepository, AUserMapper aUserMapper) {
        this.aUserRepository = aUserRepository;
        this.aUserMapper = aUserMapper;
    }

    @Override
    public AUserDTO save(AUserDTO aUserDTO) {
        log.debug("Request to save AUser : {}", aUserDTO);
        AUser aUser = aUserMapper.toEntity(aUserDTO);
        aUser = aUserRepository.save(aUser);
        return aUserMapper.toDto(aUser);
    }

    @Override
    public AUserDTO update(AUserDTO aUserDTO) {
        log.debug("Request to save AUser : {}", aUserDTO);
        AUser aUser = aUserMapper.toEntity(aUserDTO);
        aUser = aUserRepository.save(aUser);
        return aUserMapper.toDto(aUser);
    }

    @Override
    public Optional<AUserDTO> partialUpdate(AUserDTO aUserDTO) {
        log.debug("Request to partially update AUser : {}", aUserDTO);

        return aUserRepository
            .findById(aUserDTO.getId())
            .map(existingAUser -> {
                aUserMapper.partialUpdate(existingAUser, aUserDTO);

                return existingAUser;
            })
            .map(aUserRepository::save)
            .map(aUserMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AUserDTO> findAll() {
        log.debug("Request to get all AUsers");
        return aUserRepository.findAll().stream().map(aUserMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AUserDTO> findOne(Long id) {
        log.debug("Request to get AUser : {}", id);
        return aUserRepository.findById(id).map(aUserMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AUser : {}", id);
        aUserRepository.deleteById(id);
    }
}
