package com.mycoopcycle.service;

import com.mycoopcycle.service.dto.DeliveryDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.mycoopcycle.domain.Delivery}.
 */
public interface DeliveryService {
    /**
     * Save a delivery.
     *
     * @param deliveryDTO the entity to save.
     * @return the persisted entity.
     */
    DeliveryDTO save(DeliveryDTO deliveryDTO);

    /**
     * Updates a delivery.
     *
     * @param deliveryDTO the entity to update.
     * @return the persisted entity.
     */
    DeliveryDTO update(DeliveryDTO deliveryDTO);

    /**
     * Partially updates a delivery.
     *
     * @param deliveryDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DeliveryDTO> partialUpdate(DeliveryDTO deliveryDTO);

    /**
     * Get all the deliveries.
     *
     * @return the list of entities.
     */
    List<DeliveryDTO> findAll();
    /**
     * Get all the DeliveryDTO where Basket is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<DeliveryDTO> findAllWhereBasketIsNull();

    /**
     * Get the "id" delivery.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DeliveryDTO> findOne(Long id);

    /**
     * Delete the "id" delivery.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
