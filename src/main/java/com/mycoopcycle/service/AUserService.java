package com.mycoopcycle.service;

import com.mycoopcycle.service.dto.AUserDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.mycoopcycle.domain.AUser}.
 */
public interface AUserService {
    /**
     * Save a aUser.
     *
     * @param aUserDTO the entity to save.
     * @return the persisted entity.
     */
    AUserDTO save(AUserDTO aUserDTO);

    /**
     * Updates a aUser.
     *
     * @param aUserDTO the entity to update.
     * @return the persisted entity.
     */
    AUserDTO update(AUserDTO aUserDTO);

    /**
     * Partially updates a aUser.
     *
     * @param aUserDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<AUserDTO> partialUpdate(AUserDTO aUserDTO);

    /**
     * Get all the aUsers.
     *
     * @return the list of entities.
     */
    List<AUserDTO> findAll();

    /**
     * Get the "id" aUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AUserDTO> findOne(Long id);

    /**
     * Delete the "id" aUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
