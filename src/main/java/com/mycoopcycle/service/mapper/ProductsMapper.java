package com.mycoopcycle.service.mapper;

import com.mycoopcycle.domain.Delivery;
import com.mycoopcycle.domain.Products;
import com.mycoopcycle.service.dto.DeliveryDTO;
import com.mycoopcycle.service.dto.ProductsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Products} and its DTO {@link ProductsDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProductsMapper extends EntityMapper<ProductsDTO, Products> {
    @Mapping(target = "areIn", source = "areIn", qualifiedByName = "deliveryId")
    ProductsDTO toDto(Products s);

    @Named("deliveryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryDTO toDtoDeliveryId(Delivery delivery);
}
