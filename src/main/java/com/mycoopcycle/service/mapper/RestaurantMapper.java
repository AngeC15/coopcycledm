package com.mycoopcycle.service.mapper;

import com.mycoopcycle.domain.Cooperative;
import com.mycoopcycle.domain.Products;
import com.mycoopcycle.domain.Restaurant;
import com.mycoopcycle.service.dto.CooperativeDTO;
import com.mycoopcycle.service.dto.ProductsDTO;
import com.mycoopcycle.service.dto.RestaurantDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Restaurant} and its DTO {@link RestaurantDTO}.
 */
@Mapper(componentModel = "spring")
public interface RestaurantMapper extends EntityMapper<RestaurantDTO, Restaurant> {
    @Mapping(target = "offer", source = "offer", qualifiedByName = "productsId")
    @Mapping(target = "belongs", source = "belongs", qualifiedByName = "cooperativeId")
    RestaurantDTO toDto(Restaurant s);

    @Named("productsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProductsDTO toDtoProductsId(Products products);

    @Named("cooperativeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CooperativeDTO toDtoCooperativeId(Cooperative cooperative);
}
