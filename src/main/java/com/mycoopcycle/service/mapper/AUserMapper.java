package com.mycoopcycle.service.mapper;

import com.mycoopcycle.domain.AUser;
import com.mycoopcycle.domain.Basket;
import com.mycoopcycle.domain.Cooperative;
import com.mycoopcycle.domain.Restaurant;
import com.mycoopcycle.service.dto.AUserDTO;
import com.mycoopcycle.service.dto.BasketDTO;
import com.mycoopcycle.service.dto.CooperativeDTO;
import com.mycoopcycle.service.dto.RestaurantDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AUser} and its DTO {@link AUserDTO}.
 */
@Mapper(componentModel = "spring")
public interface AUserMapper extends EntityMapper<AUserDTO, AUser> {
    @Mapping(target = "possesses", source = "possesses", qualifiedByName = "restaurantId")
    @Mapping(target = "restaurant", source = "restaurant", qualifiedByName = "restaurantId")
    @Mapping(target = "areIn", source = "areIn", qualifiedByName = "cooperativeId")
    @Mapping(target = "lead", source = "lead", qualifiedByName = "cooperativeId")
    @Mapping(target = "own", source = "own", qualifiedByName = "basketId")
    AUserDTO toDto(AUser s);

    @Named("restaurantId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    RestaurantDTO toDtoRestaurantId(Restaurant restaurant);

    @Named("cooperativeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CooperativeDTO toDtoCooperativeId(Cooperative cooperative);

    @Named("basketId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    BasketDTO toDtoBasketId(Basket basket);
}
