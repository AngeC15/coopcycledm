package com.mycoopcycle.service.mapper;

import com.mycoopcycle.domain.Bank;
import com.mycoopcycle.domain.Delivery;
import com.mycoopcycle.service.dto.BankDTO;
import com.mycoopcycle.service.dto.DeliveryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Bank} and its DTO {@link BankDTO}.
 */
@Mapper(componentModel = "spring")
public interface BankMapper extends EntityMapper<BankDTO, Bank> {
    @Mapping(target = "owns", source = "owns", qualifiedByName = "deliveryId")
    BankDTO toDto(Bank s);

    @Named("deliveryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryDTO toDtoDeliveryId(Delivery delivery);
}
