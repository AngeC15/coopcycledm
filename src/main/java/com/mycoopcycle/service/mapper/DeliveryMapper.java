package com.mycoopcycle.service.mapper;

import com.mycoopcycle.domain.AUser;
import com.mycoopcycle.domain.Delivery;
import com.mycoopcycle.service.dto.AUserDTO;
import com.mycoopcycle.service.dto.DeliveryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Delivery} and its DTO {@link DeliveryDTO}.
 */
@Mapper(componentModel = "spring")
public interface DeliveryMapper extends EntityMapper<DeliveryDTO, Delivery> {
    @Mapping(target = "creator", source = "creator", qualifiedByName = "aUserId")
    DeliveryDTO toDto(Delivery s);

    @Named("aUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AUserDTO toDtoAUserId(AUser aUser);
}
