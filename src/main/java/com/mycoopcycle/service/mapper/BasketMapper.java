package com.mycoopcycle.service.mapper;

import com.mycoopcycle.domain.Basket;
import com.mycoopcycle.domain.Delivery;
import com.mycoopcycle.domain.Products;
import com.mycoopcycle.service.dto.BasketDTO;
import com.mycoopcycle.service.dto.DeliveryDTO;
import com.mycoopcycle.service.dto.ProductsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Basket} and its DTO {@link BasketDTO}.
 */
@Mapper(componentModel = "spring")
public interface BasketMapper extends EntityMapper<BasketDTO, Basket> {
    @Mapping(target = "delivery", source = "delivery", qualifiedByName = "deliveryId")
    @Mapping(target = "choosen", source = "choosen", qualifiedByName = "productsId")
    BasketDTO toDto(Basket s);

    @Named("deliveryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryDTO toDtoDeliveryId(Delivery delivery);

    @Named("productsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProductsDTO toDtoProductsId(Products products);
}
