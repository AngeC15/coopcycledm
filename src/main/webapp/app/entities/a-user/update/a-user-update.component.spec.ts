import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AUserService } from '../service/a-user.service';
import { IAUser, AUser } from '../a-user.model';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { RestaurantService } from 'app/entities/restaurant/service/restaurant.service';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { CooperativeService } from 'app/entities/cooperative/service/cooperative.service';
import { IBasket } from 'app/entities/basket/basket.model';
import { BasketService } from 'app/entities/basket/service/basket.service';

import { AUserUpdateComponent } from './a-user-update.component';

describe('AUser Management Update Component', () => {
  let comp: AUserUpdateComponent;
  let fixture: ComponentFixture<AUserUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let aUserService: AUserService;
  let restaurantService: RestaurantService;
  let cooperativeService: CooperativeService;
  let basketService: BasketService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AUserUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AUserUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AUserUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    aUserService = TestBed.inject(AUserService);
    restaurantService = TestBed.inject(RestaurantService);
    cooperativeService = TestBed.inject(CooperativeService);
    basketService = TestBed.inject(BasketService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Restaurant query and add missing value', () => {
      const aUser: IAUser = { id: 456 };
      const possesses: IRestaurant = { id: 37327 };
      aUser.possesses = possesses;
      const restaurant: IRestaurant = { id: 95641 };
      aUser.restaurant = restaurant;

      const restaurantCollection: IRestaurant[] = [{ id: 81664 }];
      jest.spyOn(restaurantService, 'query').mockReturnValue(of(new HttpResponse({ body: restaurantCollection })));
      const additionalRestaurants = [possesses, restaurant];
      const expectedCollection: IRestaurant[] = [...additionalRestaurants, ...restaurantCollection];
      jest.spyOn(restaurantService, 'addRestaurantToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aUser });
      comp.ngOnInit();

      expect(restaurantService.query).toHaveBeenCalled();
      expect(restaurantService.addRestaurantToCollectionIfMissing).toHaveBeenCalledWith(restaurantCollection, ...additionalRestaurants);
      expect(comp.restaurantsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Cooperative query and add missing value', () => {
      const aUser: IAUser = { id: 456 };
      const areIn: ICooperative = { id: 34992 };
      aUser.areIn = areIn;
      const lead: ICooperative = { id: 88904 };
      aUser.lead = lead;

      const cooperativeCollection: ICooperative[] = [{ id: 93131 }];
      jest.spyOn(cooperativeService, 'query').mockReturnValue(of(new HttpResponse({ body: cooperativeCollection })));
      const additionalCooperatives = [areIn, lead];
      const expectedCollection: ICooperative[] = [...additionalCooperatives, ...cooperativeCollection];
      jest.spyOn(cooperativeService, 'addCooperativeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aUser });
      comp.ngOnInit();

      expect(cooperativeService.query).toHaveBeenCalled();
      expect(cooperativeService.addCooperativeToCollectionIfMissing).toHaveBeenCalledWith(cooperativeCollection, ...additionalCooperatives);
      expect(comp.cooperativesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Basket query and add missing value', () => {
      const aUser: IAUser = { id: 456 };
      const own: IBasket = { id: 83129 };
      aUser.own = own;

      const basketCollection: IBasket[] = [{ id: 9115 }];
      jest.spyOn(basketService, 'query').mockReturnValue(of(new HttpResponse({ body: basketCollection })));
      const additionalBaskets = [own];
      const expectedCollection: IBasket[] = [...additionalBaskets, ...basketCollection];
      jest.spyOn(basketService, 'addBasketToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aUser });
      comp.ngOnInit();

      expect(basketService.query).toHaveBeenCalled();
      expect(basketService.addBasketToCollectionIfMissing).toHaveBeenCalledWith(basketCollection, ...additionalBaskets);
      expect(comp.basketsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const aUser: IAUser = { id: 456 };
      const possesses: IRestaurant = { id: 14422 };
      aUser.possesses = possesses;
      const restaurant: IRestaurant = { id: 93127 };
      aUser.restaurant = restaurant;
      const areIn: ICooperative = { id: 97853 };
      aUser.areIn = areIn;
      const lead: ICooperative = { id: 86942 };
      aUser.lead = lead;
      const own: IBasket = { id: 8695 };
      aUser.own = own;

      activatedRoute.data = of({ aUser });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(aUser));
      expect(comp.restaurantsSharedCollection).toContain(possesses);
      expect(comp.restaurantsSharedCollection).toContain(restaurant);
      expect(comp.cooperativesSharedCollection).toContain(areIn);
      expect(comp.cooperativesSharedCollection).toContain(lead);
      expect(comp.basketsSharedCollection).toContain(own);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AUser>>();
      const aUser = { id: 123 };
      jest.spyOn(aUserService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ aUser });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: aUser }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(aUserService.update).toHaveBeenCalledWith(aUser);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AUser>>();
      const aUser = new AUser();
      jest.spyOn(aUserService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ aUser });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: aUser }));
      saveSubject.complete();

      // THEN
      expect(aUserService.create).toHaveBeenCalledWith(aUser);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AUser>>();
      const aUser = { id: 123 };
      jest.spyOn(aUserService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ aUser });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(aUserService.update).toHaveBeenCalledWith(aUser);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackRestaurantById', () => {
      it('Should return tracked Restaurant primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRestaurantById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCooperativeById', () => {
      it('Should return tracked Cooperative primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCooperativeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackBasketById', () => {
      it('Should return tracked Basket primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackBasketById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
