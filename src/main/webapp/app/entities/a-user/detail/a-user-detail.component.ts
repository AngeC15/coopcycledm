import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAUser } from '../a-user.model';

@Component({
  selector: 'jhi-a-user-detail',
  templateUrl: './a-user-detail.component.html',
})
export class AUserDetailComponent implements OnInit {
  aUser: IAUser | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ aUser }) => {
      this.aUser = aUser;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
