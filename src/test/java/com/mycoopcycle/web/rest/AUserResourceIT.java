package com.mycoopcycle.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycoopcycle.IntegrationTest;
import com.mycoopcycle.domain.AUser;
import com.mycoopcycle.repository.AUserRepository;
import com.mycoopcycle.service.dto.AUserDTO;
import com.mycoopcycle.service.mapper.AUserMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AUserResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AUserResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SURNAME = "AAAAAAAAAA";
    private static final String UPDATED_SURNAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "0033..-(0)496807129";
    private static final String UPDATED_PHONE_NUMBER = "0033(0)-519.736870";

    private static final String ENTITY_API_URL = "/api/a-users";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AUserRepository aUserRepository;

    @Autowired
    private AUserMapper aUserMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAUserMockMvc;

    private AUser aUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AUser createEntity(EntityManager em) {
        AUser aUser = new AUser().name(DEFAULT_NAME).surname(DEFAULT_SURNAME).email(DEFAULT_EMAIL).phoneNumber(DEFAULT_PHONE_NUMBER);
        return aUser;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AUser createUpdatedEntity(EntityManager em) {
        AUser aUser = new AUser().name(UPDATED_NAME).surname(UPDATED_SURNAME).email(UPDATED_EMAIL).phoneNumber(UPDATED_PHONE_NUMBER);
        return aUser;
    }

    @BeforeEach
    public void initTest() {
        aUser = createEntity(em);
    }

    @Test
    @Transactional
    void createAUser() throws Exception {
        int databaseSizeBeforeCreate = aUserRepository.findAll().size();
        // Create the AUser
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);
        restAUserMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isCreated());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeCreate + 1);
        AUser testAUser = aUserList.get(aUserList.size() - 1);
        assertThat(testAUser.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAUser.getSurname()).isEqualTo(DEFAULT_SURNAME);
        assertThat(testAUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAUser.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void createAUserWithExistingId() throws Exception {
        // Create the AUser with an existing ID
        aUser.setId(1L);
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);

        int databaseSizeBeforeCreate = aUserRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAUserMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAUsers() throws Exception {
        // Initialize the database
        aUserRepository.saveAndFlush(aUser);

        // Get all the aUserList
        restAUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)));
    }

    @Test
    @Transactional
    void getAUser() throws Exception {
        // Initialize the database
        aUserRepository.saveAndFlush(aUser);

        // Get the aUser
        restAUserMockMvc
            .perform(get(ENTITY_API_URL_ID, aUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(aUser.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.surname").value(DEFAULT_SURNAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER));
    }

    @Test
    @Transactional
    void getNonExistingAUser() throws Exception {
        // Get the aUser
        restAUserMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAUser() throws Exception {
        // Initialize the database
        aUserRepository.saveAndFlush(aUser);

        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();

        // Update the aUser
        AUser updatedAUser = aUserRepository.findById(aUser.getId()).get();
        // Disconnect from session so that the updates on updatedAUser are not directly saved in db
        em.detach(updatedAUser);
        updatedAUser.name(UPDATED_NAME).surname(UPDATED_SURNAME).email(UPDATED_EMAIL).phoneNumber(UPDATED_PHONE_NUMBER);
        AUserDTO aUserDTO = aUserMapper.toDto(updatedAUser);

        restAUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, aUserDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isOk());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
        AUser testAUser = aUserList.get(aUserList.size() - 1);
        assertThat(testAUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAUser.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testAUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAUser.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void putNonExistingAUser() throws Exception {
        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();
        aUser.setId(count.incrementAndGet());

        // Create the AUser
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, aUserDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAUser() throws Exception {
        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();
        aUser.setId(count.incrementAndGet());

        // Create the AUser
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAUser() throws Exception {
        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();
        aUser.setId(count.incrementAndGet());

        // Create the AUser
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAUserMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAUserWithPatch() throws Exception {
        // Initialize the database
        aUserRepository.saveAndFlush(aUser);

        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();

        // Update the aUser using partial update
        AUser partialUpdatedAUser = new AUser();
        partialUpdatedAUser.setId(aUser.getId());

        partialUpdatedAUser.surname(UPDATED_SURNAME).email(UPDATED_EMAIL);

        restAUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAUser.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAUser))
            )
            .andExpect(status().isOk());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
        AUser testAUser = aUserList.get(aUserList.size() - 1);
        assertThat(testAUser.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAUser.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testAUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAUser.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void fullUpdateAUserWithPatch() throws Exception {
        // Initialize the database
        aUserRepository.saveAndFlush(aUser);

        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();

        // Update the aUser using partial update
        AUser partialUpdatedAUser = new AUser();
        partialUpdatedAUser.setId(aUser.getId());

        partialUpdatedAUser.name(UPDATED_NAME).surname(UPDATED_SURNAME).email(UPDATED_EMAIL).phoneNumber(UPDATED_PHONE_NUMBER);

        restAUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAUser.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAUser))
            )
            .andExpect(status().isOk());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
        AUser testAUser = aUserList.get(aUserList.size() - 1);
        assertThat(testAUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAUser.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testAUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAUser.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    void patchNonExistingAUser() throws Exception {
        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();
        aUser.setId(count.incrementAndGet());

        // Create the AUser
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, aUserDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAUser() throws Exception {
        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();
        aUser.setId(count.incrementAndGet());

        // Create the AUser
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAUser() throws Exception {
        int databaseSizeBeforeUpdate = aUserRepository.findAll().size();
        aUser.setId(count.incrementAndGet());

        // Create the AUser
        AUserDTO aUserDTO = aUserMapper.toDto(aUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAUserMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(aUserDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AUser in the database
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAUser() throws Exception {
        // Initialize the database
        aUserRepository.saveAndFlush(aUser);

        int databaseSizeBeforeDelete = aUserRepository.findAll().size();

        // Delete the aUser
        restAUserMockMvc
            .perform(delete(ENTITY_API_URL_ID, aUser.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AUser> aUserList = aUserRepository.findAll();
        assertThat(aUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
